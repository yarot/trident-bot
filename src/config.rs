use {
    serde_json as json,
    serenity::{
        client::bridge::gateway::{ShardId, ShardManager},
        prelude::TypeMapKey
    },
    std::collections::{HashMap, HashSet},
    std::fs::File,
    std::io::{Read, Write, Error, ErrorKind},
    std::sync::{Arc},
    tokio::sync::Mutex,
};

#[derive(Deserialize, Serialize, PartialEq)]
pub struct Config {
    pub token: String,
    pub prefix: String,
    pub members: Vec<String>,
}

impl Config {
    pub const PATH: &'static str = "settings.json";

    pub fn load(mut self) -> std::io::Result<Self> {
        let mut conf = String::new();
        let mut file = File::open(Self::PATH)?;

        if let Err(why) = (&file).metadata() {
            return Result::Err(why);
        }

        if self == Self::default() {
            file.read_to_string(&mut conf)?;
            self = json::from_str(conf.as_str())?;

            return Result::Ok(self);
        } else {
            eprintln!("The structure is the default.");

            return Result::Err(Error::new(ErrorKind::Other, "The structure is the default."));
        }
    }

    pub fn save(&self) -> std::io::Result<()> {
        let mut conf = json::to_string(self)?;
        let mut file = File::create(Self::PATH)?;

        if let Err(why) = file.write(&conf.as_bytes()) {
            eprintln!("Error writing to file!\n{}", why);

            return Result::Err(why);
        }

        return Result::Ok(());
    }
}

impl Default for Config {
    fn default() -> Config {
        Config {
            token: String::default(),
            prefix: String::default(),
            members: vec![]
        }
    }
}

pub struct CommandCounter;

impl TypeMapKey for CommandCounter {
    type Value = HashMap<String, u64>;
}

pub struct ShardManagerContainer;

impl TypeMapKey for ShardManagerContainer {
    type Value = Arc<Mutex<ShardManager>>;
}
