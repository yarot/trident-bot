//! Commands for the bot

pub use {
    self::chat::CHAT_GROUP,
    self::general::{GENERAL_GROUP, TRI_HELP},
};

pub mod chat;
pub mod general;
pub mod owner;
