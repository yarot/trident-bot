//! Commands in the "general" group.

use {
    serenity::{
        model::{
            channel::Message,
            id::UserId,
        },
        framework::standard::{
            Args, CheckResult, CommandOptions, CommandResult, CommandGroup,
            DispatchError, HelpOptions, help_commands, StandardFramework,
            macros::{command, group, help, check, hook},
        },
        prelude::Context,
        utils::{content_safe, ContentSafeOptions},
    },
    std::collections::HashSet,
};

#[group]
#[commands(ping)]
pub struct General;

#[command]
async fn ping(ctx: &Context, msg: &Message) -> CommandResult {
    msg.reply(ctx, "Pong!").await?;

    CommandResult::Ok(())
}

#[help]
#[individual_command_tip =
"Hello! こんにちは！Hola! Bonjour! 您好!\n\
If you want more information about a specific command, just pass the command as argument."]
#[command_not_found_text = "Could not find command: {}."]
#[indention_prefix = "+"]
#[lacking_permissions = "Hide"]
#[wrong_channel = "Strike"]
async fn tri_help(
    context: &Context,
    msg: &Message,
    args: Args,
    help_options: &'static HelpOptions,
    groups: &[&'static CommandGroup],
    owners: HashSet<UserId>
) -> CommandResult {
    let _ = help_commands::with_embeds(context, msg, args, help_options, groups, owners).await;


    
    CommandResult::Ok(())
}
