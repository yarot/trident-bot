//! Bot program for the Trident Discord server

#[macro_use] extern crate serde;
#[macro_use] extern crate serde_json;
#[macro_use] extern crate serenity;

use {
    self::config::{CommandCounter, Config, ShardManagerContainer},
    serenity::{
        async_trait,
        model::gateway::Ready, 
        client::Client,
        framework::standard::StandardFramework,
        http::Http,
        prelude::*,
        Result,
    },
    std::collections::{HashMap, HashSet},
    std::sync::Arc,
};

pub mod cmd;

#[doc(hidden)]
pub mod config;

pub struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, _: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);
    }
}

/// Entry point for the bot.
#[tokio::main]
async fn main() -> Result<()> {
    let config = Config::default().load()?;
    let http = Http::new_with_token(&config.token);

    let (owners, appid) = match http.get_current_application_info().await {
        Result::Ok(info) => {
            let mut owners = HashSet::new();
            owners.insert(info.owner.id);

            (owners, info.id)
        },
        Result::Err(why) => {
            eprintln!("Could not access application info: {:?}", why);

            return Result::Err(why);
        }
    };

    let framework = StandardFramework::new()
        .configure(|c| {
            c.with_whitespace(true)
            .on_mention(Some(appid))
            .prefix("tri ")
            // In this case, if "," would be first, a message would never
            // be delimited at ", ", forcing you to trim your arguments if you
            // want to avoid whitespaces at the start of each.
            .delimiters(vec![", ", ","])
            // Sets the bot's owners. These will be used for commands that
            // are owners only.
            .owners(owners)
        })
        .help(&cmd::TRI_HELP)
        .group(&cmd::CHAT_GROUP)
        .group(&cmd::GENERAL_GROUP);

    
    let mut client = Client::new(&config.token)
        .event_handler(Handler)
        .framework(framework)
        .await?;

    {
        let mut data = client.data.write().await;
        data.insert::<CommandCounter>(HashMap::default());
        data.insert::<ShardManagerContainer>(Arc::clone(&client.shard_manager));
    }

    if let Result::Err(why) = client.start().await {
        eprintln!("Error starting client: {}", why);
        
        return Result::Err(why);
    }
    
    return Result::Ok(());
}
